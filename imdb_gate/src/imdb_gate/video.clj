;;; Authors: Javier Antonio Gonzalez Trejo, Elizabeth Villanueva Rosas,
;;; Israel Faustino Cruz
(ns imdb-gate.video
  (:require [clojure.string :as str]
            [clojure.data.json :as json])
  (:gen-class))

(defmulti get-video-parameters
  "Gets the needed information from the json response"
  :Error)
(defmethod get-video-parameters "Video not found!"
  [video-json]
  {:status 404
   :headers {"Content-Type" "application/json"}
   :body (json/write-str
          video-json)})

(defmethod get-video-parameters nil
  [video-json]
  {:status 200
   :headers {"Content-Type" "application/json"}
   :body (json/write-str
          {:name (:Title video-json)
           :type (:Type video-json)
           :genre (:Genre video-json)
           :imdb_rating (-> (filter #(= (:Source %)
                                        "Internet Movie Database")
                                    (:Ratings video-json))
                            first
                            :Value
                            (str/replace #"/10" "")
                            Double.)
           :links {:href (str "/content/name/"
                              (ring.util.codec/form-encode (:Title video-json)))
                   :rel "get video details"
                   :method "GET"}})})
