;;; Authors: Javier Antonio Gonzalez Trejo, Elizabeth Villanueva Rosas,
;;; Israel Faustino Cruz
(ns imdb-gate.core
  (:require [org.httpkit.server :as server]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer :all]
            [org.httpkit.client :as http]
            [imdb-gate.video :refer [get-video-parameters]]
            [clojure.data.json :as json])
  (:gen-class))

(def omdb-href
  "http://www.omdbapi.com/")

(def omdb-query-paramaters
  "Contains the API needed to access the imbd service"
  {:timeout 200
   :query-params {:apikey "3f16bbb3"}})

(defroutes app-routes
  (GET "/video/name/:name" [name]
    (try (let [query-paramters (assoc-in omdb-query-paramaters [:query-params :t]
                                         name)
               video-json (-> @(http/get omdb-href query-paramters)
                              :body
                              (json/read-str :key-fn keyword))]
           (get-video-parameters video-json))
         (catch java.lang.NullPointerException e
           {:status 503})))
  (route/not-found "Error, page not found!"))

; Our main entry function
(defn -main
  "This is our main entry point"
  [& args]
  (let [port (Integer/parseInt (or (System/getenv "PORT") "8085"))]
                                        ; Run the server with Ring.defaults middleware
    (server/run-server (wrap-defaults #'app-routes site-defaults) {:port port})
                                        ; Run the server without ring defaults
                                        ;(server/run-server #'app-routes {:port port})
    (println (str "Running webserver at http:/127.0.0.1:" port "/"))))
