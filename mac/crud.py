# Authors: Javier Antonio Gonzalez Trejo, Elizabeth Villanueva Rosas,
# Israel Faustino Cruz
from models import db, OriginalContent
from flask_api import status
from flask import request, Blueprint
from flask_expects_json import expects_json
import requests

routes = Blueprint('crud_routes', __name__)


@routes.route('/netflix/original-content', methods=['GET'])
def get_videos():
    """Gets a list of original content"""
    genre = request.args.get('genre')
    type = request.args.get('type')
    imdb_rating = request.args.get('imdb_rating')

    videos = find_video({"imdb_rating": imdb_rating, "genre": genre,
                         "type": type})

    return {'videos': [video_representation(video) for video in videos]},\
        status.HTTP_200_OK


@routes.route('/netflix/original-content/<int:id>', methods=['GET'])
def get_video(id):
    """Gets information about a specific video"""
    data = OriginalContent.query.get(id)
    if data is None:
        return {'response': 'Not Found'}, status.HTTP_404_NOT_FOUND
    return {'video': video_representation(data)}, status.HTTP_200_OK


@routes.route('/netflix/original-content/<int:id>', methods=['PATCH'])
@expects_json(OriginalContent.schema)
def modify_video(id):
    """Mofifies an specific video"""
    json_content = request.json
    data = OriginalContent.query.get(id)
    if data is None:
        return {'response': 'Not Found'}, status.HTTP_404_NOT_FOUND

    if "name" in json_content:
        data.name = json_content["name"]
    else:
        data.name = data.name
    if "type" in json_content:
        data.type = json_content["type"]
    else:
        data.type = data.type
    if "genre" in json_content:
        data.genre = json_content["genre"]
    else:
        data.genre = data.genre
    if "imdb_rating" in json_content:
        data.imdb_rating = json_content["imdb_rating"]
    else:
        data.imdb_rating = data.imdb_rating

    db.session.commit()

    return {'video': video_representation(data)}, status.HTTP_200_OK


name_schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string", "maxLength": 100},
        "type": {"type": "string", "maxLength": 100},
        "genre": {"type": "string", "maxLength": 100},
        "imdb_rating": {"type": "number",
                        "minimum": 0,
                        "maximum": 10}
    },
    "required": ["name"]
}


@routes.route('/netflix/original-content', methods=['POST'])
@expects_json(name_schema)
def add_video():
    """Adds a video with either information of the microservice videos or
    information submmitted by the client"""
    json_content = request.json

    try:
        response = requests.get("http://localhost:8085/video/name/" +
                                json_content["name"])
    except requests.exceptions.ConnectionError:
        return '', status.HTTP_503_SERVICE_UNAVAILABLE

    if response.status_code == 404:
        return '', status.HTTP_404_NOT_FOUND

    if response.status_code == 503:
        return '', status.HTTP_503_SERVICE_UNAVAILABLE

    video_json = response.json()

    video = find_video({"name": video_json["name"]})
    if len(video) != 0:
        return {"response": "Video exists!",
                "links": links_representation(video[0])}

    video = OriginalContent()

    video.name = video_json["name"]
    if "type" in json_content:
        video.type = json_content["type"]
    else:
        video.type = video_json["type"]
    if "genre" in json_content:
        video.genre = json_content["genre"]
    else:
        video.genre = video_json["genre"]
    if "imdb_rating" in json_content:
        video.imdb_rating = json_content["imdb_rating"]
    else:
        video.imdb_rating = video_json["imdb_rating"]

    db.session.add(video)
    db.session.commit()
    return {'video': video_representation(video)}, status.HTTP_201_CREATED


def find_video(video_parameters):
    videos = OriginalContent.query

    if "genre" in video_parameters and video_parameters["genre"]:
        videos = videos.filter(
            OriginalContent.genre.like(
                "%{}%".format(video_parameters["genre"])))

    if "name" in video_parameters and video_parameters["name"]:
        videos = videos.filter_by(name=video_parameters["name"])

    if "type" in video_parameters and video_parameters["type"]:
        videos = videos.filter(
            OriginalContent.type.like(
                "%{}%".format(video_parameters["type"])))

    if "imdb_rating" in video_parameters and video_parameters["imdb_rating"]:
        videos = videos.filter(
            OriginalContent.imdb_rating.like(
                "%{}%".format(video_parameters["imdb_rating"])))

    return videos.all()


def links_representation(video):
    "Creates a dictionary with all the possible endpoints"
    temp = [{'rel': "get",
             'href': "/netflix/original-content/" + str(video.id),
             "method": "GET"},
            {'rel': "modify",
             'href': "/netflix/original-content/" + str(video.id),
             "method": "PATCH"}]
    return temp


def video_representation(video):
    """Creates a dictionary with the video parameters"""
    temp_dict = {}
    temp_dict['id'] = video.id
    temp_dict['name'] = video.name
    temp_dict['type'] = video.type
    temp_dict['genre'] = video.genre
    temp_dict['imdb_rating'] = video.imdb_rating
    temp_dict['links'] = links_representation(video)
    return temp_dict
