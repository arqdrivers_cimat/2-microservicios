# Authors: Javier Antonio Gonzalez Trejo, Elizabeth Villanueva Rosas,
# Israel Faustino Cruz
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class OriginalContent(db.Model):
    """Data model of the table original_content"""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text(), default=None, nullable=False)
    type = db.Column(db.Text(), default=None, nullable=False)
    genre = db.Column(db.Text(), default=None, nullable=False)
    imdb_rating = db.Column(db.Float())
    schema = {
            "type": "object",
            "properties": {
                "name": {"type": "string", "maxLength": 100},
                "type": {"type": "string", "maxLength": 100},
                "genre": {"type": "string", "maxLength": 100},
                "imdb_rating": {"type": "number",
                                "minimum": 0,
                                "maximum": 10}
            },
        }
