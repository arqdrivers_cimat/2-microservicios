# Authors: Javier Antonio Gonzalez Trejo, Elizabeth Villanueva Rosas,
# Israel Faustino Cruz
from flask_api import FlaskAPI
from models import db
from crud import routes

app = FlaskAPI(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///original_content.db'
db.init_app(app)
app.register_blueprint(routes)


if __name__ == '__main__':
    # Se ejecuta el servicio definiendo el host '0.0.0.0' 
    #  para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port='8084', debug=True)
